# Screenshots

## IDE Options
![Screenshot of IDE Options](ide-options.png "Screenshot of IDE Options")

## Certificate Picker
![Screenshot of Certificate Picker](certificate-picker.png "Screenshot of Certificate Picker")

## IDE Messages
![Screenshot of IDE Messages](lazarus-ide-messages-window.png "Screenshot of IDE Messages")

## Project menu item
![Screenshot of IDE menu item](project-menu-item.png "Screenshot of IDE menu item")



