program CodeSigningToolSettings;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
   cthreads, {$ENDIF} {$ENDIF}
   Interfaces, // this includes the LCL widgetset
   Forms, CodeSigningToolSettings.Form.Main, CodeSigningHelper.ProjectOptions,
   CodeSigningHelper.Menu;

{$R *.res}

begin
   RequireDerivedFormResource := True;
   Application.Scaled := True;
   Application.Initialize;
   Application.CreateForm(TFormCodeSigningFormTests, FormCodeSigningFormTests);
   Application.Run;
end.



