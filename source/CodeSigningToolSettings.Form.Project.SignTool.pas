unit CodeSigningToolSettings.Form.Project.SignTool;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ButtonPanel,
   ExtCtrls,
   CodeSigningHelper.Profiles,
   CodeSigningHelper.ProjectSettings;

type

   { TFormMicrosoftSignToolProject }

   TFormMicrosoftSignToolProject = class(TForm)
      ButtonPanel1: TButtonPanel;
      cbPageHashing: TCheckBox;
      cbSignToolProfile: TComboBox;
      cbWindowsSystemComponentVerification: TCheckBox;
      cbBuildModes: TCheckGroup;
      editURL: TEdit;
      groupExecutable: TGroupBox;
      groupURL: TGroupBox;
      groupMoreOptions: TGroupBox;
      procedure cbSignToolProfileChange({%H-}Sender: TObject);
   private
      procedure FillProfiles(AProfiles: TCodeSigningProfiles);
      procedure FillBuildModes;
      procedure UpdateOKButtonAvailability;
      procedure LoadBuildModes(AList: TStrings);
      procedure SaveBuildModes(AList: TStrings);
   public
      procedure LoadFromSettings(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings);
      procedure SaveToSettings(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings);
      function Execute(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;
   end;

function ShowMicrosoftSignToolSettings(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;

implementation

uses
   LazIDEIntf,
   IdeIntf;

function ShowMicrosoftSignToolSettings(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;
var
   form: TFormMicrosoftSignToolProject;
begin
   form := TFormMicrosoftSignToolProject.Create(nil);
   try
      Result := form.Execute(ASettings, AProfiles);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormMicrosoftSignToolProject }

procedure TFormMicrosoftSignToolProject.cbSignToolProfileChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProject.FillProfiles(AProfiles: TCodeSigningProfiles);
var
   i: integer;
begin
   cbSignToolProfile.Items.BeginUpdate;
   try
      cbSignToolProfile.Items.Clear;
      for i := 0 to Pred(AProfiles.Count) do begin
         if AProfiles[i] is TMicrosoftSignToolCodeSigningProfile then begin
            cbSignToolProfile.Items.Add(AProfiles[i].ProfileName);
         end;
      end;
   finally
      cbSignToolProfile.Items.EndUpdate;
   end;
end;

procedure TFormMicrosoftSignToolProject.FillBuildModes;
var
   i: integer;
begin
   if not Assigned(LazarusIDE) then begin
      Exit;
   end;
   if not Assigned(LazarusIDE.ActiveProject) then begin
      Exit;
   end;
   if not Assigned(LazarusIDE.ActiveProject.LazBuildModes) then begin
      Exit;
   end;
   cbBuildModes.Items.Clear;
   for i := 0 to Pred(LazarusIDE.ActiveProject.LazBuildModes.Count) do begin
      cbBuildModes.Items.Add(LazarusIDE.ActiveProject.LazBuildModes.BuildModes[i].Identifier);
   end;
end;

procedure TFormMicrosoftSignToolProject.UpdateOKButtonAvailability;
begin
   ButtonPanel1.OKButton.Enabled := (cbSignToolProfile.ItemIndex > -1);
end;

procedure TFormMicrosoftSignToolProject.LoadBuildModes(AList: TStrings);
var
   i: integer;
begin
   for i := 0 to Pred(cbBuildModes.Items.Count) do begin
      cbBuildModes.Checked[i] := AList.IndexOf(cbBuildModes.Items[i]) > -1;
   end;
end;

procedure TFormMicrosoftSignToolProject.SaveBuildModes(AList: TStrings);
var
   i: integer;
begin
   AList.Clear;
   for i := 0 to Pred(cbBuildModes.Items.Count) do begin
      if cbBuildModes.Checked[i] then begin
         AList.Add(cbBuildModes.Items[i]);
      end;
   end;
end;

procedure TFormMicrosoftSignToolProject.LoadFromSettings(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings);
begin
   cbSignToolProfile.ItemIndex := cbSignToolProfile.Items.IndexOf(ASettings.ProfileName);
   editURL.Text := ASettings.DescriptionURL;
   LoadBuildModes(ASettings.BuildModes);
   cbPageHashing.Checked := ASettings.UsePageHashing;
   cbWindowsSystemComponentVerification.Checked := ASettings.UseWindowsSystemComponenVerification;
end;

procedure TFormMicrosoftSignToolProject.SaveToSettings(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings);
begin
   if cbSignToolProfile.ItemIndex > -1 then begin
      ASettings.ProfileName := cbSignToolProfile.Items[cbSignToolProfile.ItemIndex];
   end;
   ASettings.DescriptionURL := editURL.Text;
   SaveBuildModes(ASettings.BuildModes);
   ASettings.UsePageHashing := cbPageHashing.Checked;
   ASettings.UseWindowsSystemComponenVerification := cbWindowsSystemComponentVerification.Checked;
end;

function TFormMicrosoftSignToolProject.Execute(const ASettings: TMicrosoftSignToolCodeSigningCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;
begin
   FillProfiles(AProfiles);
   FillBuildModes;
   LoadFromSettings(ASettings);
   UpdateOKButtonAvailability;
   Result := (ShowModal = mrOk);
   if Result then begin
      SaveToSettings(ASettings);
   end;
end;

end.
