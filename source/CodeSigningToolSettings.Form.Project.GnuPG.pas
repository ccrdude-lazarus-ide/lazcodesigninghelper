unit CodeSigningToolSettings.Form.Project.GnuPG;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   ButtonPanel,
   CodeSigningHelper.Profiles,
   CodeSigningHelper.ProjectSettings;

type

   { TFormGnuPGProject }

   TFormGnuPGProject = class(TForm)
      ButtonPanel1: TButtonPanel;
      cbSignToolProfile: TComboBox;
      cbASCII: TCheckBox;
      groupExecutable: TGroupBox;
      groupMoreOptions: TGroupBox;
      procedure cbSignToolProfileChange({%H-}Sender: TObject);
   private
      procedure FillProfiles(AProfiles: TCodeSigningProfiles);
      procedure UpdateOKButtonAvailability;
   public
      procedure LoadFromSettings(const ASettings: TGnuPGCompilerSettings);
      procedure SaveToSettings(const ASettings: TGnuPGCompilerSettings);
      function Execute(const ASettings: TGnuPGCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;
   end;

function ShowGnuPGSettings(const ASettings: TGnuPGCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;

implementation

function ShowGnuPGSettings(const ASettings: TGnuPGCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;
var
   form: TFormGnuPGProject;
begin
   form := TFormGnuPGProject.Create(nil);
   try
      Result := form.Execute(ASettings, AProfiles);
   finally
      form.Free;
   end;
end;

{$R *.lfm}

{ TFormGnuPGProject }

procedure TFormGnuPGProject.cbSignToolProfileChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormGnuPGProject.FillProfiles(AProfiles: TCodeSigningProfiles);
var
   i: integer;
begin
   cbSignToolProfile.Items.BeginUpdate;
   try
      cbSignToolProfile.Items.Clear;
      for i := 0 to Pred(AProfiles.Count) do begin
         if AProfiles[i] is TGnuPGCodeSigningProfile then begin
            cbSignToolProfile.Items.Add(AProfiles[i].ProfileName);
         end;
      end;
   finally
      cbSignToolProfile.Items.EndUpdate;
   end;
end;

procedure TFormGnuPGProject.UpdateOKButtonAvailability;
begin
   ButtonPanel1.OKButton.Enabled := (cbSignToolProfile.ItemIndex > -1);
end;

procedure TFormGnuPGProject.LoadFromSettings(const ASettings: TGnuPGCompilerSettings);
begin
   cbSignToolProfile.ItemIndex := cbSignToolProfile.Items.IndexOf(ASettings.ProfileName);
   cbASCII.Checked := ASettings.UseASCIIArmor;
end;

procedure TFormGnuPGProject.SaveToSettings(const ASettings: TGnuPGCompilerSettings);
begin
   if cbSignToolProfile.ItemIndex > -1 then begin
      ASettings.ProfileName := cbSignToolProfile.Items[cbSignToolProfile.ItemIndex];
   end;
   ASettings.UseASCIIArmor := cbASCII.Checked;
end;

function TFormGnuPGProject.Execute(const ASettings: TGnuPGCompilerSettings; AProfiles: TCodeSigningProfiles): boolean;
begin
   FillProfiles(AProfiles);
   LoadFromSettings(ASettings);
   UpdateOKButtonAvailability;
   Result := (ShowModal = mrOk);
   if Result then begin
      SaveToSettings(ASettings);
   end;
end;

end.
