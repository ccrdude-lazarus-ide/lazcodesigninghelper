unit CodeSigningHelper.Frame.Profiles;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   ComCtrls,
   {$IFDEF MSWindows}
   CommCtrl,
   {$ENDIF MSWindows}
   ActnList,
   Menus,
   CodeSigningHelper.Profiles;

type

   { TCodeSigningListItem }

   TCodeSigningListItem = class(TListItem)
   private
      FProfile: TCustomCodeSigningProfile;
      procedure SetProfile(AValue: TCustomCodeSigningProfile);
   public
      procedure RefreshItems;
      property Profile: TCustomCodeSigningProfile read FProfile write SetProfile;
   end;

   { TFrameCodeSigningProfiles }

   TFrameCodeSigningProfiles = class(TFrame)
      aAddSignTool: TAction;
      aAddGnuPG: TAction;
      aEdit: TAction;
      aRemove: TAction;
      alProfiles: TActionList;
      lvProfiles: TListView;
      MenuItem1: TMenuItem;
      MenuItem2: TMenuItem;
      pmiAddGnuPG: TMenuItem;
      pmipAddGnuPG: TMenuItem;
      pmiEdit: TMenuItem;
      pmiRemove: TMenuItem;
      pmiAddSignTool: TMenuItem;
      pmipAddSignTool: TMenuItem;
      pmAdd: TPopupMenu;
      pmList: TPopupMenu;
      toolbarProfiles: TToolBar;
      tbAdd: TToolButton;
      tbRemove: TToolButton;
      tbEdit: TToolButton;
      procedure aAddGnuPGExecute({%H-}Sender: TObject);
      procedure aAddSignToolExecute({%H-}Sender: TObject);
      procedure aEditExecute({%H-}Sender: TObject);
      procedure aRemoveExecute({%H-}Sender: TObject);
      procedure lvProfilesCreateItemClass({%H-}Sender: TCustomListView; var ItemClass: TListItemClass);
      procedure lvProfilesDblClick({%H-}Sender: TObject);
      procedure lvProfilesSelectItem({%H-}Sender: TObject; {%H-}Item: TListItem; Selected: boolean);
   private
      FProfiles: TCodeSigningProfiles;
      procedure RefreshListItem(AItem: TCodeSigningListItem);
      procedure RefreshListView;
   protected
      procedure SetParent(NewParent: TWinControl); override;
   public
      constructor Create(TheOwner: TComponent); override;
      destructor Destroy; override;
      procedure LoadProfilesFile(AFilename: string);
      procedure AssignProfiles(AProfiles: TCodeSigningProfiles);
   end;

implementation

uses
   {$IFDEF MSWindows}
   Windows,
   {$ENDIF MSWindows}
   CodeSigningToolSettings.Form.SignTool,
   CodeSigningToolSettings.Form.GnuPG;

{$R *.lfm}

{ TCodeSigningListItem }

procedure TCodeSigningListItem.SetProfile(AValue: TCustomCodeSigningProfile);
begin
   FProfile := AValue;
   RefreshItems;
end;

procedure TCodeSigningListItem.RefreshItems;

   procedure ListItemSetGroupId(const AListItem: TListItem; const AGroupId: integer);
   var
      lv: CommCtrl.TLVItem;
   begin
      ZeroMemory(@lv, SizeOf(TLVItem));
      lv.mask := LVIF_GROUPID;
      lv.iItem := AListItem.Index;
      lv.iGroup := AGroupId;
      SendMessage(Self.ListView.Handle, LVM_SETITEM, 0, lParam(@lv));
   end;

begin
   if not Assigned(FProfile) then begin
      Self.Caption := '';
      SubItems.Clear;
      Exit;
   end;
   if (FProfile is TMicrosoftSignToolCodeSigningProfile) then begin
      //ListItemSetGroupId(Self, 1);
   end;
   Self.Caption := FProfile.ProfileTypeText;
   while SubItems.Count < 2 do begin
      SubItems.Add('');
   end;
   SubItems[0] := FProfile.ProfileName;
   SubItems[1] := FProfile.GetDetails;
end;

{ TFrameCodeSigningProfiles }

procedure TFrameCodeSigningProfiles.lvProfilesCreateItemClass(Sender: TCustomListView; var ItemClass: TListItemClass);
begin
   ItemClass := TCodeSigningListItem;
end;

procedure TFrameCodeSigningProfiles.aRemoveExecute(Sender: TObject);
var
   li: TCodeSigningListItem;
begin
   lvProfiles.Items.BeginUpdate;
   try
      li := TCodeSigningListItem(lvProfiles.Selected);
      FProfiles.Remove(li.Profile);
      lvProfiles.Items.Delete(li.Index);
      FProfiles.SaveToIniFile();
   finally
      lvProfiles.Items.EndUpdate;
   end;
end;

procedure TFrameCodeSigningProfiles.aEditExecute(Sender: TObject);
var
   li: TCodeSigningListItem;
begin
   li := TCodeSigningListItem(lvProfiles.Selected);
   if Assigned(li) then begin
      if li.Profile is TMicrosoftSignToolCodeSigningProfile then begin
         if ShowMicrosoftSignToolProfile(TMicrosoftSignToolCodeSigningProfile(li.Profile)) then begin
            li.RefreshItems;
            FProfiles.SaveToIniFile();
         end;
      end else if li.Profile is TGnuPGCodeSigningProfile then begin
         if ShowGnuPGProfile(TGnuPGCodeSigningProfile(li.Profile)) then begin
            li.RefreshItems;
            FProfiles.SaveToIniFile();
         end;
      end;
   end;
end;

procedure TFrameCodeSigningProfiles.aAddSignToolExecute(Sender: TObject);
var
   p: TMicrosoftSignToolCodeSigningProfile;
   li: TCodeSigningListItem;
begin
   p := TMicrosoftSignToolCodeSigningProfile.Create;
   p.ProfileName := 'Give this a unique name';
   if ShowMicrosoftSignToolProfile(p) then begin
      FProfiles.Add(p);
      FProfiles.SaveToIniFile();
      li := TCodeSigningListItem(lvProfiles.Items.Add);
      li.Profile := p;
   end else begin
      p.Free;
   end;
end;

procedure TFrameCodeSigningProfiles.aAddGnuPGExecute(Sender: TObject);
var
   p: TGnuPGCodeSigningProfile;
   li: TCodeSigningListItem;
begin
   p := TGnuPGCodeSigningProfile.Create;
   p.ProfileName := 'Give this a unique name';
   if ShowGnuPGProfile(p) then begin
      FProfiles.Add(p);
      FProfiles.SaveToIniFile();
      li := TCodeSigningListItem(lvProfiles.Items.Add);
      li.Profile := p;
   end else begin
      p.Free;
   end;
end;

procedure TFrameCodeSigningProfiles.lvProfilesDblClick(Sender: TObject);
begin
   aEdit.Execute;
end;

procedure TFrameCodeSigningProfiles.lvProfilesSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   aEdit.Enabled := Selected;
   aRemove.Enabled := Selected;
end;

procedure TFrameCodeSigningProfiles.RefreshListItem(AItem: TCodeSigningListItem);
begin
   AItem.RefreshItems;
end;

procedure TFrameCodeSigningProfiles.RefreshListView;
var
   iProfile: integer;
   li: TCodeSigningListItem;
begin
   lvProfiles.Items.BeginUpdate;
   try
      lvProfiles.Items.Clear;
      for iProfile := 0 to Pred(FProfiles.Count) do begin
         li := TCodeSigningListItem(lvProfiles.Items.Add);
         li.Profile := FProfiles[iProfile];
      end;
   finally
      lvProfiles.Items.EndUpdate;
   end;
end;

procedure TFrameCodeSigningProfiles.SetParent(NewParent: TWinControl);
const
   LVGS_COLLAPSIBLE = $00000008;

   procedure AddGroup(const AHeader: WideString; const AGroupId: integer; AFooter: WideString);
   var
      G: TLVGroup;
   begin
      G.cbSize := SizeOf(TLVGroup);
      G.mask := LVGF_HEADER or LVGF_GROUPID;
      G.mask := G.mask or LVGF_STATE;
      G.stateMask := LVGS_COLLAPSIBLE;
      G.state := LVGS_COLLAPSIBLE;
      G.pszHeader := PWideChar(AHeader);
      G.cchHeader := Length(G.pszHeader);
      if Length(AFooter) > 0 then begin
         G.pszFooter := PWideChar(AFooter);
         G.cchFooter := Length(G.pszHeader);
         G.mask := G.mask or LVGF_FOOTER;
      end;
      G.iGroupId := AGroupId;
      ListView_InsertGroup(lvProfiles.Handle, -1, integer(@G));
   end;

begin
   inherited SetParent(NewParent);
   if Assigned(NewParent) then begin
      //ListView_EnableGroupView(lvProfiles.Handle, 1);
      //AddGroup('Microsoft SignTool', 1, '');
      //LoadProfilesFile('C:\Development\codesigning.ini');
   end;
end;

constructor TFrameCodeSigningProfiles.Create(TheOwner: TComponent);

begin
   inherited Create(TheOwner);
   FProfiles := nil;
end;

destructor TFrameCodeSigningProfiles.Destroy;
begin
   inherited Destroy;
end;

procedure TFrameCodeSigningProfiles.LoadProfilesFile(AFilename: string);
begin
   FProfiles.LoadFromIniFile(AFilename);
   RefreshListView;
end;

procedure TFrameCodeSigningProfiles.AssignProfiles(AProfiles: TCodeSigningProfiles);
begin
   FProfiles := AProfiles;
   RefreshListView;
end;

end.
