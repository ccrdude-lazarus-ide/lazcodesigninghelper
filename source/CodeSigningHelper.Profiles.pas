unit CodeSigningHelper.Profiles;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   IniFiles,
   contnrs,
   PepiMK.Signing.Base,
   PepiMK.Signing.MicrosoftSignTool;

type

   { TCustomCodeSigningProfile }

   TCustomCodeSigningProfile = class
   private
      FProfileName: string;
   public
      class function ProfileTypeText: string; virtual; abstract;
   public
      property ProfileName: string read FProfileName write FProfileName;
      function GetDetails: string; virtual; abstract;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string); virtual; abstract;
      procedure SaveToIni(AIni: TCustomIniFile); virtual; abstract;
   end;

   { TMicrosoftSignToolSource }

   TMicrosoftSignToolSource = class
   private
      FFilenamePFX: string;
      FHash: ansistring;
      FPasswordPFX: string;
      FSource: TCodeSignCertificateSource;
      FSubstring: ansistring;
   public
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string);
      property Hash: ansistring read FHash write FHash;
      property Substring: ansistring read FSubstring write FSubstring;
      property FilenamePFX: string read FFilenamePFX write FFilenamePFX;
      property PasswordPFX: string read FPasswordPFX write FPasswordPFX;
      property Source: TCodeSignCertificateSource read FSource write FSource;
   end;

   { TMicrosoftSignToolAzure }

   TMicrosoftSignToolAzure = class
   private
      FAccountName: string;
      FDlibPath: string;
      FEndpoint: string;
      FProfileName: string;
   public
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string);
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string);
      property Endpoint: string read FEndpoint write FEndpoint;
      property AccountName: string read FAccountName write FAccountName;
      property ProfileName: string read FProfileName write FProfileName;
      property DlibPath: string read FDlibPath write FDlibPath;
   end;

   { TMicrosoftSignToolCodeSigningProfile }

   TMicrosoftSignToolCodeSigningProfile = class(TCustomCodeSigningProfile)
   private
      FCrossSigningActive: boolean;
      FCrossSigningFilename: string;
      FExecutable: string;
      FKeyAzure: TMicrosoftSignToolAzure;
      FKeySHA256: TMicrosoftSignToolSource;
      FSigningMode: TMicrosoftSignToolCodeSigningMode;
      FTimestampingActive: boolean;
      FTimestampingRFC3161: boolean;
      FTimestampingServer: string;
   public
      class function ProfileTypeText: string; override;
   public
      constructor Create;
      destructor Destroy; override;
      function GetDetails: string; override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string); override;
      procedure SaveToIni(AIni: TCustomIniFile); override;
      property SigningMode: TMicrosoftSignToolCodeSigningMode read FSigningMode write FSigningMode;
      property KeySHA256: TMicrosoftSignToolSource read FKeySHA256;
      property KeyAzure: TMicrosoftSignToolAzure read FKeyAzure;
      property CrossSigningActive: boolean read FCrossSigningActive write FCrossSigningActive;
      property CrossSigningFilename: string read FCrossSigningFilename write FCrossSigningFilename;
      property TimestampingActive: boolean read FTimestampingActive write FTimestampingActive;
      property TimestampingServer: string read FTimestampingServer write FTimestampingServer;
      property TimestampingRFC3161: boolean read FTimestampingRFC3161 write FTimestampingRFC3161;
      property Executable: string read FExecutable write FExecutable;
   end;

   { TGnuPGCodeSigningProfile }

   TGnuPGCodeSigningProfile = class(TCustomCodeSigningProfile)
   private
      FExecutable: string;
      FSource: TCodeSignCertificateSource;
      FSourceHash: ansistring;
      FSourceSubstring: ansistring;
   public
      class function ProfileTypeText: string; override;
   public
      constructor Create;
      destructor Destroy; override;
      function GetDetails: string; override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string); override;
      procedure SaveToIni(AIni: TCustomIniFile); override;
      property SourceHash: ansistring read FSourceHash write FSourceHash;
      property SourceSubstring: ansistring read FSourceSubstring write FSourceSubstring;
      property Source: TCodeSignCertificateSource read FSource write FSource;
      property Executable: string read FExecutable write FExecutable;
   end;

   { TCodeSigningProfiles }

   TCodeSigningProfiles = class(TObjectList)
   private
      FFilename: string;
      function GetItem(AIndex: integer): TCustomCodeSigningProfile;
      procedure LoadFromIni(AIni: TCustomIniFile);
      procedure SaveToIni(AIni: TCustomIniFile);
   public
      procedure LoadFromIniFile(AFilename: string);
      procedure SaveToIniFile(AFilename: string = '');
      function FindProfile(AProfileName: string): TCustomCodeSigningProfile;
      property Items[AIndex: integer]: TCustomCodeSigningProfile read GetItem; default;
   end;


implementation

{ TMicrosoftSignToolAzure }

procedure TMicrosoftSignToolAzure.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   Self.Endpoint := AIni.ReadString(ASection, 'ACS.Endpoint', 'https://weu.codesigning.azure.net/');
   Self.AccountName := AIni.ReadString(ASection, 'ACS.AccountName', '');
   Self.ProfileName := AIni.ReadString(ASection, 'ACS.ProfileName', '');
   Self.DLibPath := AIni.ReadString(ASection, 'ACS.DLibPath', '');
end;

procedure TMicrosoftSignToolAzure.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteString(ASection, 'ACS.Endpoint', Self.Endpoint);
   AIni.WriteString(ASection, 'ACS.AccountName', Self.AccountName);
   AIni.WriteString(ASection, 'ACS.ProfileName', Self.ProfileName);
   AIni.WriteString(ASection, 'ACS.DLibPath', Self.DLibPath);
end;

{ TMicrosoftSignToolSource }

procedure TMicrosoftSignToolSource.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   Self.FilenamePFX := AIni.ReadString(ASection, 'SourceFilenamePFXSHA256', '');
   Self.PasswordPFX := AIni.ReadString(ASection, 'SourcePasswordPFXSHA256', '');
   Self.Hash := AIni.ReadString(ASection, 'SourceHashSHA256', '');
   Self.Source := TCodeSignCertificateSource(AIni.ReadInteger(ASection, 'SourceSHA256', 0));
   Self.Substring := AIni.ReadString(ASection, 'SourceSubStringSHA256', '');
end;

procedure TMicrosoftSignToolSource.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteString(ASection, 'SourceFilenamePFXSHA256', Self.FilenamePFX);
   AIni.WriteString(ASection, 'SourcePasswordPFXSHA256', Self.PasswordPFX);
   AIni.WriteString(ASection, 'SourceHashSHA256', Self.Hash);
   AIni.WriteInteger(ASection, 'SourceSHA256', integer(Self.Source));
   AIni.WriteString(ASection, 'SourceSubStringSHA256', Self.Substring);
end;

{ TCodeSigningProfiles }

function TCodeSigningProfiles.GetItem(AIndex: integer): TCustomCodeSigningProfile;
begin
   Result := TCustomCodeSigningProfile(inherited Get(AIndex));
end;

procedure TCodeSigningProfiles.LoadFromIni(AIni: TCustomIniFile);
var
   slSections: TStringList;
   iSection: integer;
   sType: string;
   pSignTool: TMicrosoftSignToolCodeSigningProfile;
   pGnuPG: TGnuPGCodeSigningProfile;
begin
   Clear;
   slSections := TStringList.Create;
   try
      AIni.ReadSections(slSections);
      for iSection := 0 to Pred(slSections.Count) do begin
         sType := AIni.ReadString(slSections[iSection], 'Type', '');
         case sType of
            'SignTool':
            begin
               pSignTool := TMicrosoftSignToolCodeSigningProfile.Create;
               pSignTool.ProfileName := slSections[iSection];
               pSignTool.LoadFromIni(AIni, slSections[iSection]);
               Self.Add(pSignTool);
            end;
            'GnuPG':
            begin
               pGnuPG := TGnuPGCodeSigningProfile.Create;
               pGnuPG.ProfileName := slSections[iSection];
               pGnuPG.LoadFromIni(AIni, slSections[iSection]);
               Self.Add(pGnuPG);
            end;
         end;
      end;
   finally
      slSections.Free;
   end;
end;

procedure TCodeSigningProfiles.SaveToIni(AIni: TCustomIniFile);
var
   slSections: TStringList;
   iSection: integer;
begin
   slSections := TStringList.Create;
   try
      AIni.ReadSections(slSections);
      for iSection := 0 to Pred(slSections.Count) do begin
         AIni.EraseSection(slSections[iSection]);
      end;
   finally
      slSections.Free;
   end;
   for iSection := 0 to Pred(Self.Count) do begin
      Items[iSection].SaveToIni(AIni);
   end;
end;

procedure TCodeSigningProfiles.LoadFromIniFile(AFilename: string);
var
   mif: TMemIniFile;
begin
   FFilename := AFilename;
   mif := TMemIniFile.Create(AFilename);
   try
      LoadFromIni(mif);
   finally
      mif.Free;
   end;
end;

procedure TCodeSigningProfiles.SaveToIniFile(AFilename: string);
var
   mif: TMemIniFile;
begin
   if Length(AFilename) = 0 then begin
      AFilename := FFilename;
   end;
   mif := TMemIniFile.Create(AFilename);
   try
      SaveToIni(mif);
      mif.UpdateFile;
   finally
      mif.Free;
   end;
end;

function TCodeSigningProfiles.FindProfile(AProfileName: string): TCustomCodeSigningProfile;
var
   i: integer;
begin
   Result := nil;
   for i := 0 to Pred(Count) do begin
      if SameText(Items[i].ProfileName, AProfileName) then begin
         Result := Items[i];
         break;
      end;
   end;
end;

{ TMicrosoftSignToolCodeSigningProfile }

class function TMicrosoftSignToolCodeSigningProfile.ProfileTypeText: string;
begin
   Result := 'MS SignTool';
end;

constructor TMicrosoftSignToolCodeSigningProfile.Create;
begin
   FKeySHA256 := TMicrosoftSignToolSource.Create;
   FKeyAzure := TMicrosoftSignToolAzure.Create;
   FSigningMode := msstcsmLocal;
end;

destructor TMicrosoftSignToolCodeSigningProfile.Destroy;
begin
   FKeySHA256.Free;
   FKeyAzure.Free;
   inherited Destroy;
end;

function TMicrosoftSignToolCodeSigningProfile.GetDetails: string;
begin
   case FSigningMode of
      msstcsmLocal: begin
         case FKeySHA256.Source of
            cscsStoreByHash: Result := 'From store by hash ' + FKeySHA256.Hash;
            cscsStoreBySubstring: Result := 'From store by substring ' + FKeySHA256.Substring;
            cscsFileAsPFX: Result := 'From file ' + ExtractFileName(FKeySHA256.FilenamePFX);
         end;
      end;
      msstcsmAzure: begin
         Result := Format('Azure Code Signing w/ account %s and profile %s', [FKeyAzure.AccountName, FKeyAzure.ProfileName]);
      end;
   end;
   if FTimestampingActive then begin
      Result += ', w/ timestamping';
   end;
   if FCrossSigningActive then begin
      Result += ', w/ cross-signing';
   end;
end;

procedure TMicrosoftSignToolCodeSigningProfile.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FExecutable := AIni.ReadString(ASection, 'Executable', 'c:\Program Files (x86)\Windows Kits\10\bin\x86\signtool.exe');
   FCrossSigningActive := AIni.ReadBool(ASection, 'CrossSigningActive', False);
   FCrossSigningFilename := AIni.ReadString(ASection, 'CrossSigningFilename', '');
   FKeySHA256.LoadFromIni(AIni, ASection);
   FKeyAzure.LoadFromIni(AIni, ASection);
   FTimestampingActive := AIni.ReadBool(ASection, 'TimestampingActive', False);
   FTimestampingRFC3161 := AIni.ReadBool(ASection, 'TimestampingRFC3161', True);
   FTimestampingServer := AIni.ReadString(ASection, 'TimestampingServer', '');

   // TMicrosoftSignToolCodeSigningMode = (msstcsmLocal, msstcsmAzure);
   FSigningMode := TMicrosoftSignToolCodeSigningMode(AIni.ReadInteger(ASection, 'SigningMode', 0));
end;

procedure TMicrosoftSignToolCodeSigningProfile.SaveToIni(AIni: TCustomIniFile);
begin
   AIni.WriteString(FProfileName, 'Type', 'SignTool');
   AIni.WriteString(FProfileName, 'Executable', FExecutable);
   AIni.WriteBool(FProfileName, 'CrossSigningActive', FCrossSigningActive);
   AIni.WriteString(FProfileName, 'CrossSigningFilename', FCrossSigningFilename);
   FKeySHA256.SaveToIni(AIni, FProfileName);
   FKeyAzure.SaveToIni(AIni, FProfileName);
   AIni.WriteBool(FProfileName, 'TimestampingActive', FTimestampingActive);
   AIni.WriteBool(FProfileName, 'TimestampingRFC3161', FTimestampingRFC3161);
   AIni.WriteString(FProfileName, 'TimestampingServer', FTimestampingServer);

   // TMicrosoftSignToolCodeSigningMode = (msstcsmLocal, msstcsmAzure);
   AIni.WriteInteger(FProfileName, 'SigningMode', integer(FSigningMode));
end;

{ TGnuPGCodeSigningProfile }

class function TGnuPGCodeSigningProfile.ProfileTypeText: string;
begin
   Result := 'GnuPG';
end;

constructor TGnuPGCodeSigningProfile.Create;
begin

end;

destructor TGnuPGCodeSigningProfile.Destroy;
begin
   inherited Destroy;
end;

function TGnuPGCodeSigningProfile.GetDetails: string;
begin
   case FSource of
      cscsStoreByHash: Result := 'From store by hash ' + FSourceHash;
      cscsStoreBySubstring: Result := 'From store by substring ' + FSourceSubstring;
   end;
end;

procedure TGnuPGCodeSigningProfile.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FExecutable := AIni.ReadString(ASection, 'Executable', 'gnupg');
   FSourceHash := AIni.ReadString(ASection, 'SourceHash', '');
   FSource := TCodeSignCertificateSource(AIni.ReadInteger(ASection, 'Source', 0));
   FSourceSubstring := AIni.ReadString(ASection, 'SourceSubString', '');
end;

procedure TGnuPGCodeSigningProfile.SaveToIni(AIni: TCustomIniFile);
begin
   AIni.WriteString(FProfileName, 'Type', 'GnuPG');
   AIni.WriteString(FProfileName, 'Executable', FExecutable);
   AIni.WriteString(FProfileName, 'SourceHash', FSourceHash);
   AIni.WriteInteger(FProfileName, 'Source', integer(FSource));
   AIni.WriteString(FProfileName, 'SourceSubString', FSourceSubstring);
end;

end.
