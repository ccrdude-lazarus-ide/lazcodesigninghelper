unit CodeSigningHelper.ProjectOptions.Settings.Frame;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   FileUtil,
   Forms,
   Controls,
   StdCtrls,
   EditBtn,
   PepiMK.Signing.Base,
   PepiMK.Signing.MicrosoftSignTool,
   CodeSigningHelper.Options,
   CodeSigningHelper.ProjectOptions,
   CodeSigningHelper.Frame.ProjectSettings,
   IDEOptEditorIntf,
   IDEOptionsIntf;

type

   { TFrameCodeSigningCompilerOptionsSettings }

   TFrameCodeSigningCompilerOptionsSettings = class(TAbstractIDEOptionsEditor)
      FrameCodeSigningProjectSettings1: TFrameCodeSigningProjectSettings;
   public
      { public declarations }
      function GetTitle: string; override;
      procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
      procedure ReadSettings(AOptions: TAbstractIDEOptions); override;
      procedure WriteSettings(AOptions: TAbstractIDEOptions); override;
      class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
   end;

implementation

{$R *.lfm}

uses
   Dialogs,
   IDEIntf,
   IDEExternToolIntf,
   CodeSigningHelper.Strings,
   CodeSigningHelper.Debug;

{ TFrameCodeSigningCompilerOptionsSettings }

function TFrameCodeSigningCompilerOptionsSettings.GetTitle: string;
begin
   Result := 'CodeSigning Settings';
end;

procedure TFrameCodeSigningCompilerOptionsSettings.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
   ReadSettings(TCodeSigningProjectOptions.GetInstance);
end;

procedure TFrameCodeSigningCompilerOptionsSettings.ReadSettings(AOptions: TAbstractIDEOptions);
var
   o: TCodeSigningProjectOptions;
begin
   CodeSigningLogInformation(Self, 'ReadSettings');
   o := TCodeSigningProjectOptions(AOptions);
   o.Read;
   FrameCodeSigningProjectSettings1.AssignSettings(o.ProjectSettings);
   FrameCodeSigningProjectSettings1.AssignProfiles(CodeSigningOptions.Profiles);
end;

procedure TFrameCodeSigningCompilerOptionsSettings.WriteSettings(AOptions: TAbstractIDEOptions);
var
   o: TCodeSigningProjectOptions;
begin
   CodeSigningLogInformation(Self, 'WriteSettings');
   o := TCodeSigningProjectOptions(AOptions);
   o.Write;
end;

class function TFrameCodeSigningCompilerOptionsSettings.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
   Result := TCodeSigningProjectOptions;
end;

initialization

   // GroupProject was wrong, since it was per-project, not per-buildmode
   // GroupCompiler needs special BuildMode treating
   RegisterIDEOptionsEditor(GroupProject, TFrameCodeSigningCompilerOptionsSettings, 805);
end.



