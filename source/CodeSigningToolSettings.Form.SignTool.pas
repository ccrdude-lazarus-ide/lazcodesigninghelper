{
   @author(Patrick Michael Kolla-ten Venne [pk] <patrick@kolla-tenvenne.de>)
   @abstract(Form with details about MS signtool codesigning profile.)

   @preformatted(
// *****************************************************************************
// Copyright: © 2017-2022 Patrick Michael Kolla-ten Venne. All rights reserved.
// *****************************************************************************
// Changelog (new entries first):
// ---------------------------------------
// 2022-05-31  pk  ---  [CCD] Updated unit header.
// *****************************************************************************
   )
}

unit CodeSigningToolSettings.Form.SignTool;

{$IFDEF FPC}
{$mode objfpc}{$H+}
{$ENDIF FPC}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   Graphics,
   Dialogs,
   StdCtrls,
   EditBtn,
   ButtonPanel,
   ComCtrls,
   ExtCtrls,
   PepiMK.Signing.Base,
   PepiMK.Signing.MicrosoftSignTool,
   CodeSigningHelper.Profiles;

type

   { TFormMicrosoftSignToolProfile }

   TFormMicrosoftSignToolProfile = class(TForm)
      ButtonPanel1: TButtonPanel;
      cbACSDlibPath: TComboBox;
      cbSigningMode: TComboBox;
      cbTimeStampingServer: TComboBox;
      cbACSEndpoint: TComboBox;
      editACSAccountName: TEdit;
      editACSProfileName: TEdit;
      editExecutable: TComboBox;
      cbUseTimeStamping: TCheckBox;
      cbXSignCert: TCheckBox;
      editKeyStoreHash: TEditButton;
      editKeySubjectSubstring: TEdit;
      editProfileName: TEdit;
      editKeyPFXFilename: TFileNameEdit;
      editKeyPFXPassword: TEdit;
      editXSignCert: TFileNameEdit;
      labelACSDLibPath: TLabel;
      labelProfileName: TLabel;
      labelSignToolExecutable: TLabel;
      labelACSAccountName: TLabel;
      labelACSProfileName: TLabel;
      labelACSEndpoint: TLabel;
      labelSignMode: TLabel;
      pcSigningMode: TPageControl;
      Panel1: TPanel;
      rbKeyStoreHash: TRadioButton;
      rbKeyPFX: TRadioButton;
      rbKeySubjectSubstring: TRadioButton;
      tabKey: TTabSheet;
      tabACS: TTabSheet;
      procedure cbACSEndpointChange({%H-}Sender: TObject);
      procedure cbSigningModeChange(Sender: TObject);
      procedure cbTimeStampingServerChange({%H-}Sender: TObject);
      procedure cbUseTimeStampingChange({%H-}Sender: TObject);
      procedure cbXSignCertChange({%H-}Sender: TObject);
      procedure editACSAccountNameChange(Sender: TObject);
      procedure editACSProfileNameChange(Sender: TObject);
      procedure editKeyStoreHashButtonClick({%H-}Sender: TObject);
      procedure editKeyStoreHashChange({%H-}Sender: TObject);
      procedure editKeySubjectSubstringChange({%H-}Sender: TObject);
      procedure editExecutableChange({%H-}Sender: TObject);
      procedure editKeyPFXFilenameChange({%H-}Sender: TObject);
      procedure editProfileNameChange({%H-}Sender: TObject);
      procedure editXSignCertChange({%H-}Sender: TObject);
      procedure FormShow({%H-}Sender: TObject);
      procedure rbKeyStoreHashChange({%H-}Sender: TObject);
      procedure rbKeyPFXChange({%H-}Sender: TObject);
      procedure rbKeySubjectSubstringChange({%H-}Sender: TObject);
   private
      procedure LocateSignToolExeutables;
      procedure UpdateOKButtonAvailability;
   public
      procedure LoadFromProfile(const AProfile: TMicrosoftSignToolCodeSigningProfile);
      procedure SaveToProfile(const AProfile: TMicrosoftSignToolCodeSigningProfile);
      function Execute(const AProfile: TMicrosoftSignToolCodeSigningProfile): boolean;
   end;

function ShowMicrosoftSignToolProfile(const AProfile: TMicrosoftSignToolCodeSigningProfile): boolean;

implementation

{$R *.lfm}

uses
   WinDirs,
   CodeSigningHelper.Certificates.Form;

function ShowMicrosoftSignToolProfile(const AProfile: TMicrosoftSignToolCodeSigningProfile): boolean;
var
   form: TFormMicrosoftSignToolProfile;
begin
   form := TFormMicrosoftSignToolProfile.Create(nil);
   try
      Result := form.Execute(AProfile);
   finally
      form.Free;
   end;
end;

{ TFormMicrosoftSignToolProfile }

procedure TFormMicrosoftSignToolProfile.editKeyStoreHashButtonClick(Sender: TObject);
var
   s: string;
begin
   s := editKeyStoreHash.Text;
   if SelectWindowsCertificate(s) then begin
      editKeyStoreHash.Text := s;
      UpdateOKButtonAvailability;
   end;
end;

procedure TFormMicrosoftSignToolProfile.cbXSignCertChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editACSAccountNameChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editACSProfileNameChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;

end;

procedure TFormMicrosoftSignToolProfile.cbUseTimeStampingChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.cbTimeStampingServerChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.cbACSEndpointChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;

end;

procedure TFormMicrosoftSignToolProfile.cbSigningModeChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editKeyStoreHashChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editKeySubjectSubstringChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editExecutableChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editKeyPFXFilenameChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editProfileNameChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.editXSignCertChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.FormShow(Sender: TObject);
begin
   case cbSigningMode.ItemIndex of
      0: tabKey.Show;
      1: tabACS.Show;
   end;
end;

procedure TFormMicrosoftSignToolProfile.rbKeyStoreHashChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.rbKeyPFXChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.rbKeySubjectSubstringChange(Sender: TObject);
begin
   UpdateOKButtonAvailability;
end;

procedure TFormMicrosoftSignToolProfile.LocateSignToolExeutables;

   procedure AddIfExists(AFilename: string);
   begin
      if FileExists(AFilename) then begin
         editExecutable.Items.Add(AFilename);
      end;
   end;

begin
   editExecutable.Items.BeginUpdate;
   try
      editExecutable.Items.Clear;
      // 'c:\Program Files (x86)\Windows Kits\10\bin\x86\signtool.exe'
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\Tools\bin\i386\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\App Certification Kit\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.14393.0\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.14393.0\x64\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.15063.0\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.15063.0\x64\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.16299.0\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.16299.0\x64\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.17134.0\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.17134.0\x64\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.19041.0\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.19041.0\x64\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.22000.0\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\10.0.22000.0\x64\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFilesX86)) + 'Windows Kits\10\bin\x86\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFiles)) + 'Microsoft Corporation\Microsoft Platform Ready Test Tool 4.1\signtool.exe');
      AddIfExists(IncludeTrailingPathDelimiter(GetWindowsSpecialDir(FOLDERID_ProgramFiles)) + 'Microsoft Corporation\MPR Test Tool for Windows Server\signtool.exe');
   finally
      editExecutable.Items.EndUpdate;
   end;
end;

procedure TFormMicrosoftSignToolProfile.UpdateOKButtonAvailability;
var
   bValidSigningKey: boolean;
   bOk: boolean;
begin
   case cbSigningMode.ItemIndex of
      0: begin
         bValidSigningKey := (rbKeyStoreHash.Checked and (Length(editKeyStoreHash.Text) > 0)) // ...
            or (rbKeySubjectSubstring.Checked and (Length(editKeySubjectSubstring.Text) > 0)) // ...
            or (rbKeyPFX.Checked and (Length(editKeyPFXFilename.Text) > 0));
         if (cbXSignCert.Checked) then begin
            if Length(editXSignCert.Text) = 0 then begin
               bValidSigningKey := False;
            end;
         end;
         if (cbUseTimeStamping.Checked) then begin
            if Length(cbTimeStampingServer.Text) = 0 then begin
               bValidSigningKey := False;
            end;
         end;
         bOk := (Length(editProfileName.Text) > 0) and (Length(editExecutable.Text) > 0) and bValidSigningKey;
      end;
      1: begin
         bOk := (Length(editACSAccountName.Text) > 0) and (Length(editACSProfileName.Text) > 0) and (Length(cbACSEndpoint.Text) > 0);
      end;
   end;
   ButtonPanel1.OKButton.Enabled := bOk;
end;

procedure TFormMicrosoftSignToolProfile.LoadFromProfile(const AProfile: TMicrosoftSignToolCodeSigningProfile);
begin
   editProfileName.Text := AProfile.ProfileName;
   editExecutable.Text := AProfile.Executable;

   editKeyStoreHash.Text := AProfile.KeySHA256.Hash;
   editKeySubjectSubstring.Text := AProfile.KeySHA256.Substring;
   editKeyPFXFilename.Text := AProfile.KeySHA256.FilenamePFX;
   editKeyPFXPassword.Text := AProfile.KeySHA256.PasswordPFX;
   rbKeyStoreHash.Checked := (AProfile.KeySHA256.Source = cscsStoreByHash);
   rbKeyPFX.Checked := (AProfile.KeySHA256.Source = cscsFileAsPFX);
   rbKeySubjectSubstring.Checked := (AProfile.KeySHA256.Source = cscsStoreBySubstring);

   cbACSEndpoint.Text := AProfile.KeyAzure.Endpoint;
   editACSAccountName.Text := AProfile.KeyAzure.AccountName;
   editACSProfileName.Text := AProfile.KeyAzure.ProfileName;
   cbACSDlibPath.Text := AProfile.KeyAzure.DlibPath;

   cbSigningMode.ItemIndex := integer(AProfile.SigningMode);

   cbTimeStampingServer.Text := AProfile.TimestampingServer;
   cbUseTimeStamping.Checked := AProfile.TimestampingActive;
   cbXSignCert.Checked := AProfile.CrossSigningActive;
   editXSignCert.Text := AProfile.CrossSigningFilename;
end;

procedure TFormMicrosoftSignToolProfile.SaveToProfile(const AProfile: TMicrosoftSignToolCodeSigningProfile);
begin
   AProfile.ProfileName := editProfileName.Text;
   AProfile.Executable := editExecutable.Text;

   AProfile.KeySHA256.Hash := editKeyStoreHash.Text;
   AProfile.KeySHA256.Substring := editKeySubjectSubstring.Text;
   AProfile.KeySHA256.FilenamePFX := editKeyPFXFilename.Text;
   AProfile.KeySHA256.PasswordPFX := editKeyPFXPassword.Text;
   if rbKeyStoreHash.Checked then begin
      AProfile.KeySHA256.Source := cscsStoreByHash;
   end else if rbKeySubjectSubstring.Checked then begin
      AProfile.KeySHA256.Source := cscsStoreBySubstring;
   end else begin
      AProfile.KeySHA256.Source := cscsFileAsPFX;
   end;

   AProfile.SigningMode := TMicrosoftSignToolCodeSigningMode(cbSigningMode.ItemIndex);

   AProfile.KeyAzure.Endpoint := cbACSEndpoint.Text;
   AProfile.KeyAzure.AccountName := editACSAccountName.Text;
   AProfile.KeyAzure.ProfileName := editACSProfileName.Text;
   AProfile.KeyAzure.DlibPath := cbACSDlibPath.Text;

   AProfile.CrossSigningActive := cbXSignCert.Checked;
   AProfile.CrossSigningFilename := editXSignCert.Text;

   AProfile.TimestampingServer := cbTimeStampingServer.Text;
   AProfile.TimestampingActive := cbUseTimeStamping.Checked;
end;

function TFormMicrosoftSignToolProfile.Execute(const AProfile: TMicrosoftSignToolCodeSigningProfile): boolean;
begin
   LocateSignToolExeutables;
   LoadFromProfile(AProfile);
   UpdateOKButtonAvailability;
   Result := (ShowModal = mrOk);
   if Result then begin
      SaveToProfile(AProfile);
   end;
end;

end.
