{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit CodeSigningPackage;

{$warn 5023 off : no warning about unused units}
interface

uses
   CodeSigningHelper.Options, CodeSigningHelper.Menu, CodeSigningHelper.Strings, PepiMK.Signing.AppleCodeSign, PepiMK.Signing.Base, PepiMK.Signing.GnuPG, 
   PepiMK.Signing.MicrosoftSignTool, CodeSigningHelper.ProjectOptions, PepiMK.Signing.JavaKeyTool, CodeSigningHelper.Options.Profiles.Frame, 
   CodeSigningHelper.ProjectOptions.Settings.Frame, CodeSigningToolSettings.Form.Project.SignTool, CodeSigningToolSettings.Form.SignTool, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('CodeSigningHelper.Menu', @CodeSigningHelper.Menu.Register);
end;

initialization
  RegisterPackage('CodeSigningPackage', @Register);
end.
