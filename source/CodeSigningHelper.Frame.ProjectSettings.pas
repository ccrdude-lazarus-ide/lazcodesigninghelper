unit CodeSigningHelper.Frame.ProjectSettings;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   Forms,
   Controls,
   ComCtrls,
   ActnList,
   Menus,
   Dialogs, StdCtrls,
   CodeSigningHelper.Profiles,
   CodeSigningHelper.ProjectSettings;

type

   { TCodeSigningProjectSettingsListItem }

   TCodeSigningProjectSettingsListItem = class(TListItem)
   private
      FSettings: TCustomCodeSigningCompilerSettings;
      procedure SetProfile(AValue: TCustomCodeSigningCompilerSettings);
   public
      procedure RefreshItems;
      property Settings: TCustomCodeSigningCompilerSettings read FSettings write SetProfile;
   end;

   { TFrameCodeSigningProjectSettings }

   TFrameCodeSigningProjectSettings = class(TFrame)
      aAddSignTool: TAction;
      aAddGnuPG: TAction;
      aMoveUp: TAction;
      aMoveDown: TAction;
      aEdit: TAction;
      aRemove: TAction;
      alProjectSettings: TActionList;
      Label1: TLabel;
      lvSettings: TListView;
      pmipAddGnuPG: TMenuItem;
      pmipAddSignTool: TMenuItem;
      pmAdd: TPopupMenu;
      toolbarProfiles: TToolBar;
      tbAdd: TToolButton;
      tbEdit: TToolButton;
      tbRemove: TToolButton;
      tbMoveUp: TToolButton;
      tbMoveDown: TToolButton;
      procedure aAddGnuPGExecute({%H-}Sender: TObject);
      procedure aAddSignToolExecute({%H-}Sender: TObject);
      procedure aEditExecute({%H-}Sender: TObject);
      procedure aMoveDownExecute({%H-}Sender: TObject);
      procedure aMoveUpExecute({%H-}Sender: TObject);
      procedure aRemoveExecute({%H-}Sender: TObject);
      procedure lvSettingsCreateItemClass({%H-}Sender: TCustomListView; var ItemClass: TListItemClass);
      procedure lvSettingsDblClick({%H-}Sender: TObject);
      procedure lvSettingsSelectItem({%H-}Sender: TObject; {%H-}Item: TListItem; Selected: boolean);
   private
      FSettings: TCodeSigningCompilerSettings;
      FProfiles: TCodeSigningProfiles;
      procedure RefreshListView;
   protected
      procedure SetParent(NewParent: TWinControl); override;
   public
      procedure AssignSettings(ASettings: TCodeSigningCompilerSettings);
      procedure AssignProfiles(AProfiles: TCodeSigningProfiles);
   end;

implementation

{$R *.lfm}

uses
   CodeSigningToolSettings.Form.Project.SignTool,
   CodeSigningToolSettings.Form.Project.GnuPG;

{ TCodeSigningProjectSettingsListItem }

procedure TCodeSigningProjectSettingsListItem.SetProfile(AValue: TCustomCodeSigningCompilerSettings);
begin
   FSettings := AValue;
   RefreshItems;
end;

procedure TCodeSigningProjectSettingsListItem.RefreshItems;
var
   s: string;
begin
   if not Assigned(FSettings) then begin
      Self.Caption := '';
      SubItems.Clear;
      Exit;
   end;
   Self.Caption := FSettings.ProfileName;
   while SubItems.Count < 2 do begin
      SubItems.Add('');
   end;
   SubItems[0] := FSettings.SettingsTypeText;
   s := '';
   if FSettings is TMicrosoftSignToolCodeSigningCompilerSettings then begin
      if TMicrosoftSignToolCodeSigningCompilerSettings(FSettings).UsePageHashing then begin
         s += 'w/ page hashing';
      end;
      if Length(TMicrosoftSignToolCodeSigningCompilerSettings(FSettings).DescriptionURL) > 0 then begin
         if Length(s) > 0 then begin
            s+= ', ';
         end;
         s += 'url=' + TMicrosoftSignToolCodeSigningCompilerSettings(FSettings).DescriptionURL;
      end;
   end else if FSettings is TGnuPGCompilerSettings then begin
      if TGnuPGCompilerSettings(FSettings).UseASCIIArmor then begin
         s += 'ascii armor file';
      end else begin
         s += 'binary signature file';
      end;
   end;
   SubItems[1] := Trim(s);
end;

{ TFrameCodeSigningProjectSettings }

procedure TFrameCodeSigningProjectSettings.lvSettingsCreateItemClass(Sender: TCustomListView; var ItemClass: TListItemClass);
begin
   ItemClass := TCodeSigningProjectSettingsListItem;
end;

procedure TFrameCodeSigningProjectSettings.lvSettingsDblClick(Sender: TObject);
begin
   aEdit.Execute;
end;

procedure TFrameCodeSigningProjectSettings.lvSettingsSelectItem(Sender: TObject; Item: TListItem; Selected: boolean);
begin
   aEdit.Enabled := Selected;
   aRemove.Enabled := Selected;
   aMoveUp.Enabled := Selected;
   aMoveDown.Enabled := Selected;
end;

procedure TFrameCodeSigningProjectSettings.RefreshListView;
var
   iSetting: integer;
   li: TCodeSigningProjectSettingsListItem;
begin
   if not Assigned(FSettings) then begin
      Exit;
   end;
   lvSettings.Items.BeginUpdate;
   try
      lvSettings.Items.Clear;
      for iSetting := 0 to Pred(FSettings.Count) do begin
         li := TCodeSigningProjectSettingsListItem(lvSettings.Items.Add);
         li.Settings := FSettings[iSetting];
      end;
   finally
      lvSettings.Items.EndUpdate;
   end;
end;

procedure TFrameCodeSigningProjectSettings.SetParent(NewParent: TWinControl);
begin
   inherited SetParent(NewParent);
end;

procedure TFrameCodeSigningProjectSettings.AssignSettings(ASettings: TCodeSigningCompilerSettings);
begin
   FSettings := ASettings;
   RefreshListView;
end;

procedure TFrameCodeSigningProjectSettings.AssignProfiles(AProfiles: TCodeSigningProfiles);
begin
   FProfiles := AProfiles;
   RefreshListView;
end;

procedure TFrameCodeSigningProjectSettings.aAddSignToolExecute(Sender: TObject);
var
   p: TMicrosoftSignToolCodeSigningCompilerSettings;
   li: TCodeSigningProjectSettingsListItem;
begin
   p := TMicrosoftSignToolCodeSigningCompilerSettings.Create;
   try
      if ShowMicrosoftSignToolSettings(p, FProfiles) then begin
         FSettings.Add(p);
         FSettings.SaveToIniFile();
         li := TCodeSigningProjectSettingsListItem(lvSettings.Items.Add);
         li.Settings := p;
      end else begin
         p.Free;
      end;
   except
      on E: Exception do begin
         MessageDlg('Add SignTool profile', E.Message, mtError, [mbOK], 0);
      end;
   end;
end;

procedure TFrameCodeSigningProjectSettings.aAddGnuPGExecute(Sender: TObject);
var
   p: TGnuPGCompilerSettings;
   li: TCodeSigningProjectSettingsListItem;
begin
   p := TGnuPGCompilerSettings.Create;
   if ShowGnuPGSettings(p, FProfiles) then begin
      FSettings.Add(p);
      FSettings.SaveToIniFile();
      li := TCodeSigningProjectSettingsListItem(lvSettings.Items.Add);
      li.Settings := p;
   end else begin
      p.Free;
   end;
end;

procedure TFrameCodeSigningProjectSettings.aEditExecute(Sender: TObject);
var
   li: TCodeSigningProjectSettingsListItem;
begin
   li := TCodeSigningProjectSettingsListItem(lvSettings.Selected);
   if Assigned(li) then begin
      if li.Settings is TMicrosoftSignToolCodeSigningCompilerSettings then begin
         if ShowMicrosoftSignToolSettings(TMicrosoftSignToolCodeSigningCompilerSettings(li.Settings), FProfiles) then begin
            li.RefreshItems;
            FSettings.SaveToIniFile();
         end;
      end;
      if li.Settings is TGnuPGCompilerSettings then begin
         if ShowGnuPGSettings(TGnuPGCompilerSettings(li.Settings), FProfiles) then begin
            li.RefreshItems;
            FSettings.SaveToIniFile();
         end;
      end;
   end;
end;

procedure TFrameCodeSigningProjectSettings.aMoveDownExecute(Sender: TObject);
var
   li: TCodeSigningProjectSettingsListItem;
begin
   li := TCodeSigningProjectSettingsListItem(lvSettings.Selected);
   if Assigned(li) then begin
      FSettings.MoveDown(li.Index);
      FSettings.SaveToIniFile();
      RefreshListView;
   end;
end;

procedure TFrameCodeSigningProjectSettings.aMoveUpExecute(Sender: TObject);
var
   li: TCodeSigningProjectSettingsListItem;
begin
   li := TCodeSigningProjectSettingsListItem(lvSettings.Selected);
   if Assigned(li) then begin
      FSettings.MoveUp(li.Index);
      FSettings.SaveToIniFile();
      RefreshListView;
   end;
end;

procedure TFrameCodeSigningProjectSettings.aRemoveExecute(Sender: TObject);
var
   li: TCodeSigningProjectSettingsListItem;
begin
   lvSettings.Items.BeginUpdate;
   try
      li := TCodeSigningProjectSettingsListItem(lvSettings.Selected);
      FSettings.Remove(li.Settings);
      lvSettings.Items.Delete(li.Index);
      FSettings.SaveToIniFile();
   finally
      lvSettings.Items.EndUpdate;
   end;
end;

end.
