unit CodeSigningHelper.ProjectSettings;

{$mode objfpc}{$H+}

interface

uses
   Classes,
   SysUtils,
   IniFiles,
   contnrs,
   PepiMK.Signing.Base;

type

   { TCustomCodeSigningCompilerSettings }

   TCustomCodeSigningCompilerSettings = class
   private
      FBuildModes: TStringList;
      FAutoSign: boolean;
      FProfileName: string;
   public
      class function SettingsTypeText: string; virtual; abstract;
   public
      constructor Create;
      destructor Destroy; override;
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string); virtual;
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string); virtual;
      property ProfileName: string read FProfileName write FProfileName;
      property AutoSign: boolean read FAutoSign write FAutoSign;
      property BuildModes: TStringList read FBuildModes;
   end;

   { TMicrosoftSignToolCodeSigningCompilerSettings }

   TMicrosoftSignToolCodeSigningCompilerSettings = class(TCustomCodeSigningCompilerSettings)
   private
      FDescriptionURL: string;
      FUsePageHashing: boolean;
      FUseWindowsSystemComponenVerification: boolean;
   public
      class function SettingsTypeText: string; override;
   public
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string); override;
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string); override;
      property DescriptionURL: string read FDescriptionURL write FDescriptionURL;
      property UsePageHashing: boolean read FUsePageHashing write FUsePageHashing;
      property UseWindowsSystemComponenVerification: boolean read FUseWindowsSystemComponenVerification write FUseWindowsSystemComponenVerification;
   end;

   { TGnuPGCompilerSettings }

   TGnuPGCompilerSettings = class(TCustomCodeSigningCompilerSettings)
   private
      FUseASCIIArmor: boolean;
   public
      class function SettingsTypeText: string; override;
   public
      procedure LoadFromIni(AIni: TCustomIniFile; ASection: string); override;
      procedure SaveToIni(AIni: TCustomIniFile; ASection: string); override;
      property UseASCIIArmor: boolean read FUseASCIIArmor write FUseASCIIArmor;
   end;

   { TCodeSigningCompilerSettings }

   TCodeSigningCompilerSettings = class(TObjectList)
   private
      FFilename: string;
      function GetItem(AIndex: integer): TCustomCodeSigningCompilerSettings;
      procedure LoadFromIni(AIni: TCustomIniFile);
      procedure SaveToIni(AIni: TCustomIniFile);
   public
      procedure LoadFromIniFile(AFilename: string);
      procedure SaveToIniFile(AFilename: string = '');
      procedure MoveUp(AIndex: integer);
      procedure MoveDown(AIndex: integer);
      property Items[AIndex: integer]: TCustomCodeSigningCompilerSettings read GetItem; default;
   end;

implementation

{ TGnuPGCompilerSettings }

class function TGnuPGCompilerSettings.SettingsTypeText: string;
begin
   Result := 'GnuPG';
end;

procedure TGnuPGCompilerSettings.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   inherited LoadFromIni(AIni, ASection);
   FUseASCIIArmor := AIni.ReadBool(ASection, 'UseASCIIArmor', False);
end;

procedure TGnuPGCompilerSettings.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   inherited SaveToIni(AIni, ASection);
   AIni.WriteString(ASection, 'Type', 'GnuPG');
   AIni.WriteBool(ASection, 'UseASCIIArmor', FUseASCIIArmor);
end;

{ TCodeSigningCompilerSettings }

function TCodeSigningCompilerSettings.GetItem(AIndex: integer): TCustomCodeSigningCompilerSettings;
begin
   Result := TCustomCodeSigningCompilerSettings(inherited Get(AIndex));
end;

procedure TCodeSigningCompilerSettings.LoadFromIni(AIni: TCustomIniFile);
var
   i: integer;
   iCount: integer;
   sType: string;
   psSignTool: TMicrosoftSignToolCodeSigningCompilerSettings;
   psGnuPG: TGnuPGCompilerSettings;
begin
   Clear;
   iCount := AIni.ReadInteger('Main', 'Count', 0);
   for i := 0 to Pred(iCount) do begin
      sType := AIni.ReadString(IntToStr(i), 'Type', '');
      case sType of
         'SignTool':
         begin
            psSignTool := TMicrosoftSignToolCodeSigningCompilerSettings.Create;
            psSignTool.LoadFromIni(AIni, IntToStr(i));
            Add(psSignTool);
         end;
         'GnuPG':
         begin
            psGnuPG := TGnuPGCompilerSettings.Create;
            psGnuPG.LoadFromIni(AIni, IntToStr(i));
            Add(psGnuPG);
         end;
      end;
   end;
end;

procedure TCodeSigningCompilerSettings.SaveToIni(AIni: TCustomIniFile);
var
   slSections: TStringList;
   iSection: integer;
begin
   slSections := TStringList.Create;
   try
      AIni.ReadSections(slSections);
      for iSection := Pred(slSections.Count) downto 0 do begin
         AIni.EraseSection(slSections[iSection]);
      end;
   finally
      slSections.Free;
   end;
   AIni.WriteInteger('Main', 'Count', Self.Count);
   for iSection := 0 to Pred(Self.Count) do begin
      Items[iSection].SaveToIni(AIni, IntToStr(iSection));
   end;
end;

procedure TCodeSigningCompilerSettings.LoadFromIniFile(AFilename: string);
var
   mif: TMemIniFile;
begin
   FFilename := AFilename;
   mif := TMemIniFile.Create(AFilename);
   try
      LoadFromIni(mif);
   finally
      mif.Free;
   end;
end;

procedure TCodeSigningCompilerSettings.SaveToIniFile(AFilename: string);
var
   mif: TMemIniFile;
begin
   if Length(AFilename) = 0 then begin
      AFilename := FFilename;
   end;
   mif := TMemIniFile.Create(AFilename);
   try
      SaveToIni(mif);
      mif.UpdateFile;
   finally
      mif.Free;
   end;
end;

procedure TCodeSigningCompilerSettings.MoveUp(AIndex: integer);
begin
   if AIndex < 1 then begin
      Exit;
   end;
   Self.Move(AIndex, AIndex - 1);
end;

procedure TCodeSigningCompilerSettings.MoveDown(AIndex: integer);
begin
   if AIndex >= Pred(Count) then begin
      Exit;
   end;
   Self.Move(AIndex, AIndex + 1);
end;

{ TCustomCodeSigningCompilerSettings }

constructor TCustomCodeSigningCompilerSettings.Create;
begin
   FBuildModes := TStringList.Create;
   FBuildModes.StrictDelimiter := true;
   FBuildModes.Delimiter := '|';
end;

destructor TCustomCodeSigningCompilerSettings.Destroy;
begin
   FBuildModes.Free;
   inherited Destroy;
end;

procedure TCustomCodeSigningCompilerSettings.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   FProfileName := AIni.ReadString(ASection, 'ProfileName', '');
   FAutoSign := AIni.ReadBool(ASection, 'AutoSign', True);
   FBuildModes.DelimitedText := AIni.ReadString(ASection, 'BuildModes', '');
end;

procedure TCustomCodeSigningCompilerSettings.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   AIni.WriteString(ASection, 'ProfileName', FProfileName);
   AIni.WriteBool(ASection, 'AutoSign', FAutoSign);
   AIni.WriteString(ASection, 'BuildModes', FBuildModes.DelimitedText);
end;

{ TMicrosoftSignToolCodeSigningCompilerSettings }

class function TMicrosoftSignToolCodeSigningCompilerSettings.SettingsTypeText: string;
begin
   Result := 'Microsoft SignTool';
end;

procedure TMicrosoftSignToolCodeSigningCompilerSettings.LoadFromIni(AIni: TCustomIniFile; ASection: string);
begin
   inherited LoadFromIni(AIni, ASection);
   FDescriptionURL := AIni.ReadString(ASection, 'DescriptionURL', '');
   FUsePageHashing := AIni.ReadBool(ASection, 'UsePageHashing', True);
   FUseWindowsSystemComponenVerification := AIni.ReadBool(ASection, 'UseWindowsSystemComponenVerification', False);
end;

procedure TMicrosoftSignToolCodeSigningCompilerSettings.SaveToIni(AIni: TCustomIniFile; ASection: string);
begin
   inherited SaveToIni(AIni, ASection);
   AIni.WriteString(ASection, 'Type', 'SignTool');
   AIni.WriteString(ASection, 'DescriptionURL', FDescriptionURL);
   AIni.WriteBool(ASection, 'UsePageHashing', FUsePageHashing);
   AIni.WriteBool(ASection, 'UseWindowsSystemComponenVerification', FUseWindowsSystemComponenVerification);
end;

end.
